<?php 
$GLOBALS['site_url'] = 'http://chiangmaisalad.com/';
$GLOBALS['site_url'] = 'http://localhost/salad/';

$GLOBALS['lang'] = 'th';

require_once '../header.php';
?>

    <section class="pwr-section pwr-about" id="about">
        <div class="container-fluid">
            <article class="pwr-body row">
                <div class="pwr-section-intro col-md-offset-1 col-md-10">
                    <div class="pwr-heading text-center">
                        <h1 class="pwr-heading-primary">About</h1>
                        <h2 class="pwr-heading-secondary">our story</h2>
                    </div>
                    <p class="lead">
                    สวัสดีครับทุกท่าน ก่อนอื่นผมขอแนะนำตัวผมเองก่อนครับ ผมชื่อ นายกฤช ดามนคร 
                    ผมอยากบอกเล่าความเป็นมาของ <strong class="pwr-text-green">"เชียงใหม่สลัด"</strong> โดยเกิดจากตัวผมเองที่ชอบทำอาหาร และชอบที่จะเล่นและพลิกแพลงตามที่ตัวเองชอบ 
                    และเมื่อสิบกว่าปีก่อน ผมได้ทีโอกาสเดินทางไปต่างประเทศในแถบอเมริกา และได้พบสลัดชนิดหนึ่ง ซึ่งทางร้านเขาได้นำผลไม้มาทำเป็นน้ำสลัด ซึ่งเป็นที่แปลกใหม่สำหรับผม เพราะแทบทุกโต๊ะจะต้องสั่งเมนูสลัดนี้มาทาน ผมเป็นคนหนึ่งที่ไม่พลาดในการสั่งสลัดมาทาน </p>
                    <p class="lead">
                    คำแรกที่ทานเข้าไป รับรู้ถึงความอร่อยเป็นพิเศษ สมองผมเริ่มคิดว่า (เอ่...) น้ำสลัดนี้มีอะไรเป็นส่วนผสมบ้าง ทำไมอร่อยมาก มีรสชาติหวานอมเปรี้ยว มีกลิ่นหอม ซึ่งทำให้นึกถึงตอนสมัยเป็นเด็กที่คุณแม่ชอบทำน้ำมะขามเปียกให้ทานบ่อยๆ มีรสชาติคล้ายๆ กัน แต่น้ำสลัดนี้หอมกว่ามาก
                    </p>
                </div>
                <!-- <div class="pwr-feature">
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-1.png" width="140">
                            <div class=""><strong>Fresh</strong></div>
                        </div>   
                    </div>
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-2.png" width="140">
                            <div class=""><strong>Clean</strong></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-3.png" width="140">
                            <div class=""><strong>Delicious</strong></div>
                        </div>
                    </div>
                </div> -->
            </article>
        </div>
    </section>
    <!-- end about section -->

    <section class="pwr-section pwr-product" id="product">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Our Salad</h1>
                <h2 class="pwr-heading-secondary">Premium salad by Krit</h2>
            </div>
            <div class="pwr-section-sub">
                <p class="lead">
                    หลังจากกลับมาเมืองไทย ผมเปิดร้านอาหารอยู่แถวย่านนิมมานเหมินทร์ และได้คิดเมนูสลัดโดยใช้ชื่อเมนูว่า "เชียงใหม่สลัด" โดยนำน้ำมะขามเปียกมาเป็นส่วนประกอบ และได้ทำให้ลูกค้าชิมเวลามาทานอาหารที่ร้าน และได้รับการตอบรับจากลูกค้าว่ารสชาติอร่อย มีความหอมแปลกพิเศษ
                </p>
            </div>
            <div class="pwr-section-sub">
                <div class="pwr-gallery flex-images" id="galleryContainer">
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg">
                        </a>
                    </div>  
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg">
                        </a>
                    </div>
                    <!-- <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <img src="photos/photo-5.jpg">
                    </div> -->
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg">
                        </a>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-1.jpg">
                            <h3 class="pwr-heading">Solution one</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-2.jpg">
                            <h3 class="pwr-heading">Solution two</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-3.jpg">
                            <h3 class="pwr-heading">Solution three</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-4.jpg">
                            <h3 class="pwr-heading">Solution four</h3>
                        </div>
                    </div>
                </div> -->
            </div>
            <p class="lead">
                สำหรับผักที่นำมาทำสลัด จะใช้ผักสลัดทั่วไป และผักที่ใช้ในครัวเรือนของภาคเหนือมาผสมเข้าด้วยกัน ทำให้มีรสชาติความรู้สึกระหว่างฝรั่งกับไทย (ภาคเหนือ เชียงใหม่) ซึ่งขอรับรองว่าเมนูสลัดนี้ มีคุณค่าทางอาหารครบ 5 หมู่ เหมาะสำหรับผู้ที่รักสุขภาพ เหมาะสำหรับผู้ที่ต้องการควบคุมน้ำหนัก และเหมาะสำหรับผู้ที่ต้องการทานอาหารแบบสบายท้อง
            </p>
            <p class="lead">
                <strong class="pwr-text-green">"เชียงใหม่สลัด"</strong> เมนูนี้ ตอบโจทย์แก่ผู้ที่ชอบทานสลัด และผู้ที่มองหาเมนูสุขภาพได้เป็นอย่างดี
            </p>
        </div>
    </section>
    <!-- end product section -->

    <section class="pwr-section pwr-order" id="order">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Order</h1>
                <h2 class="pwr-heading-secondary">How to pre order</h2>
            </div>
            <div class="pwr-section-sub">
                <div class="row">
                    <div class="col-md-6">
                        <p class="">
                            เรามีบริการส่งทุกวัน โดยต้องมีการสั่งซื้อขึ้นต่ำ 200 บาท (3 กล่อง)
                        </p>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border pwr-circle-truck">
                                    <i class="fa fa-truck"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <ul>
                                    <li>สั่งล่วงหน้า 1 วัน</li>
                                    <li>ส่งฟรีภายในตัวเมือง (นอกเมืองคิดค่าบริการ 50 บาท)</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <caption>เมนูสลัด</caption>
                            <tr>
                                <td valign="middle">ปกติ (ไก่, ปลา)</td>
                                <td class="text-right"><strong class="price">79 </strong>฿</td>
                            </tr>
                            <tr>
                                <td valign="middle">พิเศษ (กุ้ง)</td>
                                <td class="text-right"><strong class="price">99 </strong>฿</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
    </section>
     <!-- end order section -->

    <section class="pwr-section pwr-salad">
    <!-- end salad -->

<?php require_once '../footer.php'; ?>