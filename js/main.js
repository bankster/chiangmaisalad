$(function() {
	$(window).scroll(function() {
		var s = $(window).scrollTop();
  		var t = '#mainNav';
  		var c = 'pwr-nav-small';
  		if (s >= 50) { $(t).addClass(c); } else { $(t).removeClass(c); }
	});

	$('body').scrollspy({ 
		target: '#siteNav',
		offset: 51
	});

	/* hide collapse after click link */
	var navMain = $("#siteNav");
    navMain.on("click", "a", null, function () {
        navMain.collapse('hide');
    });

	$('#mainNav').localScroll({offset:-50,duration:600});
	$('#scrollAbout').localScroll({offset:-50,duration:700});

	$("#galleryContainer").flexImages({
		container: '.pwr-gallery-item',
		rowHeight: 360
	});

	$('.lightbox').nivoLightbox();

	window.sr = ScrollReveal({ mobile: false });
	sr.reveal('#about');
	sr.reveal('.pwr-gallery-item', { 
		origin   : "bottom",
        distance : "200px",
        duration : 1200,
        scale    : 1,
        ease     : "ease-in-out"
	}, 100);
	sr.reveal('.pwr-salad');
	sr.reveal('.pwr-contact-info .media', {
		origin   : "left",
		distance : "50px",
		duration : 800,
		scale    : 1,
		ease     : "ease-in-out"
	}, 50);
	sr.reveal('.pwr-contact-form', {
		origin   : "right",
		distance : "50px",
		duration : 800,
		scale    : 1,
		ease     : "ease-in-out"
	});

	// AJAX Contact Form
	var cForm = $('#contactForm');
	var formMessages = $('#formMessages');

	cForm.submit(function(event) {
		event.preventDefault();

		// Serialize the form data.
		var formData = $(cForm).serialize();

		//console.log(formData);

		// Submit the form using AJAX.
		$.ajax({
		    type: 'POST',
		    url: $(cForm).attr('action'),
		    data: formData
		})
		.done(function(response) {
		    //console.log(response);

		    // Set the message text.
		    $(formMessages).text(response);

		    $('#messageModal').modal('show');

		    // Clear the form.
		    $('#name').val('');
		    $('#email').val('');
		    $('#message').val('');
		})
		.fail(function(data) {
		    // Set the message text.
		    if (data.responseText !== '') {
		        $(formMessages).text(data.responseText);
		    } else {
		        $(formMessages).text('Oops! An error occured and your message could not be sent.');
		    }

		    $('#messageModal').modal('show');
		});

	});
});