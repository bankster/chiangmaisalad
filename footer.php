    </div>
    <!-- end .page -->
    <footer class="pwr-section pwr-footer pwr-contact" id="contact">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Contact</h1>
            </div>
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <div class="pwr-contact-info">
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border">
                                    <i class="fa fa-home"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <span>Chiang Mai Salad by Krit</span>
                                <address>59/8 Nimmanhaemin Rd, A.Muang, Chiang Mai, Thailand 50200</address>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                            <div class="media-body media-body-middle">
                                +66 081-998-7165
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border">
                                    <i class="fa fa-mobile"></i>
                                </div>
                            </div>
                            <div class="media-body media-body-middle">
                                Line ID : cmsalad
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                            </div>
                            <div class="media-body media-body-middle">
                                krta2567nakron@gmail.com
                            </div>
                        </div>
                    </div>
                    <!-- end pwr-contact-info -->
                </div>
                <div class="col-md-5">
                    <div class="pwr-contact-form">
                        <form method="post" action="<?php echo $GLOBALS['site_url']; ?>send_mail.php" class="" id="contactForm">
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Your Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Your Email" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" class="form-control" rows="5" placeholder="Message" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn pwr-btn-contact">Sent Message</button>
                            </div>
                        </form>
                    </div>
                </div>          
            </div>
        </div>
    </footer>
    <!-- end contact footer -->

    <!-- Modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                </div>
                <div class="modal-body" id="formMessages"></div>
            </div>
        </div>
    </div>



    <script src="<?php echo $GLOBALS['site_url']; ?>js/vendor/jquery-1.12.0.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>bower_components/jquery.scrollTo/jquery.scrollTo.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>bower_components/jquery.localScroll/jquery.localScroll.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>bower_components/scrollreveal/dist/scrollreveal.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>js/vendor/jquery.flex-images.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>bower_components/nivo-lightbox/nivo-lightbox.min.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>js/plugins.js"></script>
    <script src="<?php echo $GLOBALS['site_url']; ?>js/main.js"></script>

    </body>
</html>