<?php 
$GLOBALS['site_url'] = 'http://chiangmaisalad.com/';
//$GLOBALS['site_url'] = 'http://localhost/salad/';

$GLOBALS['lang'] = 'ch';

require_once '../header.php';
?>

    <section class="pwr-section pwr-about" id="about">
        <div class="container-fluid">
            <article class="pwr-body row">
                <div class="pwr-section-intro col-md-offset-1 col-md-10">
                    <div class="pwr-heading text-center">
                        <h1 class="pwr-heading-primary">About</h1>
                        <h2 class="pwr-heading-secondary">our story</h2>
                    </div>
                    <p class="lead">
                        各位好！首先我先自我介绍，我叫Krit Tamnakorn，我想介绍一下关于“清迈萨拉”的来源，产生于本人对烹饪非常感兴趣，经常尝试调配个人喜欢的食物。十几年前我曾经去过南非国家，那时我发现了一种萨拉，我觉得很好奇，因为那个店的萨拉酱是用水果做的，每桌客户都会点这种萨拉，也我包括我在内。尝试的一口使我感觉到很特别的味道，当时我在想这种萨拉有些什么调料呢，为什么味道这么好，酸酸甜甜、香香的味道，让我回想到小时候妈妈常常给我做的罗望子汁，味道相似，不过该萨拉的味道更香。
                    </p>
                </div>
                <!-- <div class="pwr-feature">
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-1.png" width="140">
                            <div class=""><strong>Fresh</strong></div>
                        </div>   
                    </div>
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-2.png" width="140">
                            <div class=""><strong>Clean</strong></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pwr-feature-item">
                            <img src="img/icon-3.png" width="140">
                            <div class=""><strong>Delicious</strong></div>
                        </div>
                    </div>
                </div> -->
            </article>
        </div>
    </section>
    <!-- end about section -->

    <section class="pwr-section pwr-product" id="product">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Our Salad</h1>
                <h2 class="pwr-heading-secondary">Premium salad by Krit</h2>
            </div>
            <div class="pwr-section-sub">
                <p class="lead">
                    回到泰国后，我在宁曼路开了一家餐厅，并发明了一道菜叫“清迈萨拉”，我用罗望子做萨拉的调味料，到本店用餐的客户可以尝试，由于萨拉拥有非常特别的香味，因此受到了很多客户的欢迎。
                </p>
            </div>
            <div class="pwr-section-sub">
                <div class="pwr-gallery flex-images" id="galleryContainer">
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg">
                        </a>
                    </div>  
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg">
                        </a>
                    </div>
                    <!-- <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <img src="photos/photo-5.jpg">
                    </div> -->
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg">
                        </a>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-1.jpg">
                            <h3 class="pwr-heading">Solution one</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-2.jpg">
                            <h3 class="pwr-heading">Solution two</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-3.jpg">
                            <h3 class="pwr-heading">Solution three</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-4.jpg">
                            <h3 class="pwr-heading">Solution four</h3>
                        </div>
                    </div>
                </div> -->
            </div>
            <p class="lead">
                用来做拉萨的蔬菜为一般的萨拉菜，此外还有泰国北部的一些蔬菜，这儿能感觉到西泰综合的味道。我保证该拉萨拥有五种主要饮食的营养，适合养生者和减肥者。
            </p>
            <p class="lead">
                “清迈拉萨”非常适合喜爱萨拉的客户，为了您的身体健康，千万别错过这道菜。
            </p>
        </div>
    </section>
    <!-- end product section -->

    <section class="pwr-section pwr-order" id="order">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Order</h1>
                <h2 class="pwr-heading-secondary">How to pre order</h2>
            </div>
            <div class="pwr-section-sub">
                <div class="row">
                    <div class="col-md-6">
                        <p class="">
                            Daily delivery  minimum order 3 boxes
                        </p>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border pwr-circle-truck">
                                    <i class="fa fa-truck"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <ul>
                                    <li>order 1 day Before</li>
                                    <li>free delivery in town, out side the city charge 50 baht</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <caption>Salad Menu</caption>
                            <tr>
                                <td valign="middle">chicken,fish</td>
                                <td class="text-right"><strong class="price">79 </strong>฿</td>
                            </tr>
                            <tr>
                                <td valign="middle">shrimps</td>
                                <td class="text-right"><strong class="price">99 </strong>฿</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
    </section>
     <!-- end order section -->

    <section class="pwr-section pwr-salad"></section>
    <!-- end salad -->

<?php require_once '../footer.php'; ?>