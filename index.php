<?php 
$GLOBALS['site_url'] = 'http://chiangmaisalad.com/';
// $GLOBALS['site_url'] = 'http://localhost/salad/';

$GLOBALS['lang'] = 'en';

require_once 'header.php';
?>

    <section class="pwr-section pwr-about" id="about">
        <div class="container-fluid">
            <article class="pwr-body row">
                <div class="pwr-section-intro col-md-offset-1 col-md-10" data-scroll-reveal="move 25px">
                    <div class="pwr-heading text-center">
                        <h1 class="pwr-heading-primary">About</h1>
                        <h2 class="pwr-heading-secondary">our story</h2>
                    </div>
                    <p class="lead">
                        Hello, everyone. Let me introduce myself: My name is Krit Tamnakorn and I would like to tell you how I created my recipe for <strong class="pwr-text-green">CHIANGMAI SALAD.</strong> First, I must with satisfaction state that I am a born cook. All my life I have invented new combinations of Thai foods as well as dishes based on recipes from different countries and cookery traditions.
                    </p>
                    <p class="lead">
                    Ten years ago I had the opportunity to travel to America. In a California restaurant I chanced upon a popular salad with a flavorful dressing. Noticing that many customers were ordering it, I decide to order the same. Then, even with the first bite I felt the unique taste of its dressing savoring my mouth. Immediately in my mind I questioned--- Why was it so delicious? What were its ingredients? What made it so special?
                    </p>
                </div>
            </article>
        </div>
    </section>
    <!-- end about section -->

    <section class="pwr-section pwr-product" id="product">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Our Salad</h1>
                <h2 class="pwr-heading-secondary">Premium salad by Krit</h2>
            </div>
            <div class="pwr-section-sub">
                <p class="lead">
                    The taste was sweet/tart and very fragrant. It reminded me of my childhood when my mother slow-cooked tamarinds with palm sugar to make a sweet-sour sauce tasting very similar to what I encountered in that California restaurant.
                </p>
            </div>
            <div class="pwr-section-sub">
                <div class="pwr-gallery flex-images" id="galleryContainer">
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-1.jpg">
                        </a>
                    </div>  
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-3.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-2.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-4.jpg">
                        </a>
                    </div>
                    <!-- <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <img src="photos/photo-5.jpg">
                    </div> -->
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-6.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-7.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="800" data-h="600">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-8.jpg">
                        </a>
                    </div>
                    <div class="pwr-gallery-item item" data-w="600" data-h="800">
                        <a href="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg" class="lightbox" data-lightbox-gallery="gallery1">
                            <img src="<?php echo $GLOBALS['site_url']; ?>photos/photo-9.jpg">
                        </a>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-1.jpg">
                            <h3 class="pwr-heading">Solution one</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-2.jpg">
                            <h3 class="pwr-heading">Solution two</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-3.jpg">
                            <h3 class="pwr-heading">Solution three</h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pwr-solution-item">
                            <img class="img-responsive img-rounded" src="img/solution-4.jpg">
                            <h3 class="pwr-heading">Solution four</h3>
                        </div>
                    </div>
                </div> -->
            </div>
            <p class="lead">
                    Later, returned to Chiang Mai, Thailand, I opened my own restaurant on Nimanhemin Road where I would experiment with a variety of sauces, often trying blends of tamarinds, palm sugar and other dressing ingredients in varying dishes to get reactions and opinions from the patrons of my restaurant. Ultimately, from those kitchen tryouts there developed my special dish which I named <strong class="pwr-text-green">CHIANGMAI SALAD.</strong>
            </p>
            <p class="lead">
                Now, apart from the healthful benefits derived from its refreshing garden vegetables, my unique <strong class="pwr-text-green">CHIANGMAI SALAD</strong> has become a captivating choice at breakfast, lunch, dinner or in fact, any time.
            </p>
        </div>
    </section>
    <!-- end product section -->

    <section class="pwr-section pwr-order" id="order">
        <div class="container-fluid">
            <div class="pwr-heading text-center">
                <h1 class="pwr-heading-primary">Order</h1>
                <h2 class="pwr-heading-secondary">How to pre order</h2>
            </div>
            <div class="pwr-section-sub">
                <div class="row">
                    <div class="col-md-6">
                        <p class="">
                            Daily delivery  minimum order 3 boxes
                        </p>
                        <div class="media">
                            <div class="media-left">
                                <div class="pwr-circle-border pwr-circle-truck">
                                    <i class="fa fa-truck"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <ul>
                                    <li>order 1 day Before</li>
                                    <li>free delivery in town, out side the city charge 50 baht</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <caption>Salad Menu</caption>
                            <tr>
                                <td valign="middle">chicken,fish</td>
                                <td class="text-right"><strong class="price">79 </strong>฿</td>
                            </tr>
                            <tr>
                                <td valign="middle">shrimps</td>
                                <td class="text-right"><strong class="price">99 </strong>฿</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
    </section>
     <!-- end order section -->

    <section class="pwr-section pwr-salad"></section>
    <!-- end salad -->

<?php require_once 'footer.php'; ?>