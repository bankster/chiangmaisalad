<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Chiangmai Salad by Krit</title>
        <meta name="description" content="">
        <meta name="keywords" content="chiangmai salad, สลัด, สลัดเชียงใหม่">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">

        <!-- favicon and app icon -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $GLOBALS['site_url']; ?>img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $GLOBALS['site_url']; ?>img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $GLOBALS['site_url']; ?>img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $GLOBALS['site_url']; ?>img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $GLOBALS['site_url']; ?>img/favicon-16x16.png">
        <link rel="manifest" href="<?php echo $GLOBALS['site_url']; ?>img/manifest.json">
        <meta name="msapplication-TileColor" content="#3daf2c">
        <meta name="msapplication-TileImage" content="<?php echo $GLOBALS['site_url']; ?>img/ms-icon-144x144.png">
        <meta name="theme-color" content="#3daf2c">
        
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>css/animate.css">
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Noto+Serif:700,700italic,400|Noto+Sans'>
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>bower_components/components-font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>css/jquery.flex-images.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>bower_components/nivo-lightbox/nivo-lightbox.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>bower_components/nivo-lightbox/themes/default/default.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>css/main.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['site_url']; ?>css/responsive.css">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="page">
        
        <header class="pwr-header">
            <nav class="navbar pwr-navbar navbar-fixed-top" id="mainNav">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#siteNav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://www.chiangmaisalad.com">
                            <div class="pwr-brand-img">
                                <img src="<?php echo $GLOBALS['site_url']; ?>img/chiangmai-salad-logo.png" alt="Logo Chiangmai Salad by Krit" class="img-responsive">
                            </div>
                            <div class="pwr-brand-text">Chiangmai Salad</div>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="siteNav">
                        <ul class="nav navbar-nav pwr-nav">
                            <li>
                                <a href="#about">About</a>
                            </li>
                            <li>
                                <a href="#product">Our Salad</a>
                            </li>
                            <li>
                                <a href="#order">Order</a>
                            </li>
                            <li>
                                <a href="#contact">Contact</a>
                            </li>
                        </ul>
                        <div class="navbar-right">
                            <div class="pwr-navbar-lang">
                                <div class="btn-group">
                                    <a href="<?php echo $GLOBALS['site_url']; ?>" class="btn pwr-btn-circle <?php if($GLOBALS['lang'] == 'en') echo 'active'; ?>">Eng</a></li>
                                    <a href="<?php echo $GLOBALS['site_url']; ?>th/" class="btn pwr-btn-circle <?php if($GLOBALS['lang'] == 'th') echo 'active'; ?>">ไทย</a>
                                    <a href="<?php echo $GLOBALS['site_url']; ?>ch/" class="btn pwr-btn-circle <?php if($GLOBALS['lang'] == 'ch') echo 'active'; ?>">汉语</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- end .pwr-nav-main -->
            <div class="pwr-hero">
                <div class="container-fluid">
                    <div class="pwr-hero-title">
                     Chiang Mai Salad by Krit
                    </div>
                    <!-- <div class="pwr-hero-desc">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </div> -->
                    <div class="pwr-hero-anchor" id="scrollAbout">
                        <a href="#about" class="btn btn-lg pwr-btn-outline-white" >
                            view our story 
                        </a>
                    </div>
                    
                </div>
               
            </div>
            <!-- end .pwr-hero -->
        </header>