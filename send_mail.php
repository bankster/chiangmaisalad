<?php
require_once "phpmailer/PHPMailerAutoload.php";

$mail_user = 'contact@chiangmaisalad.com';
$mail_pass = 'inde2554';

// Set the recipient email address.
// FIXME: Update this to your desired email address.
$recipient = "krta2567nakron@gmail.com";

// Only process POST reqeusts.
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get the form fields and remove whitespace.
    $name = strip_tags(trim($_POST["name"]));
	$name = str_replace(array("\r","\n"),array(" "," "),$name);
    $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
    $message = trim($_POST["message"]);

    // Check that data was sent to the mailer.
    if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Set a 400 (bad request) response code and exit.
        http_response_code(400);
        echo "Oops! There was a problem with your submission. Please complete the form and try again.";
        exit;
    }

    // Set the email subject.
    $subject = "New contact from $name";

    // Build the email content.
    $email_content = "Name: $name\n";
    $email_content .= "Email: $email\n\n";
    $email_content .= "Message:\n$message\n";

    // Build the email headers.
    $email_headers = "From: $name <$email>";

    $mail = new PHPMailer;
    //var_dump($mail); exit;
    $mail->isSMTP();                                // Set mailer to use SMTP
    $mail->Host = 'mail.chiangmaisalad.com';        // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                         // Enable SMTP authentication
    $mail->Username = $mail_user;                   // SMTP username
    $mail->Password = $mail_pass;                   // SMTP password

    $mail->setFrom($mail_user, 'Contact');
    $mail->addAddress($recipient);     // Add a recipient
    $mail->addAddress('iamtidz@gmail.com');     // Add a recipient

    $mail->Subject = $subject;
    $mail->Body    = $email_content;


    // Send the email.
    if ($mail->send()) {
        // Set a 200 (okay) response code.
        http_response_code(200);
        echo "Thank You! Your message has been sent.";
    } else {
        // Set a 500 (internal server error) response code.
        http_response_code(500);
        echo "Oops! Something went wrong and we couldn't send your message.";
        echo "<br>Mailer Error: " . $mail->ErrorInfo;
    }

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "There was a problem with your submission, please try again.";
}